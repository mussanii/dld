import React, { useState } from 'react'
import loginIcons from '../assest/signin.gif';
import { FaEye } from "react-icons/fa6";
import { FaEyeSlash } from "react-icons/fa";
import { Link, useNavigate } from 'react-router-dom';
import imageTobase64 from '../helpers/imageTobase64';
import SummaryApi from '../common';
import { toast } from 'react-toastify';

const SignUp = () => {
    const [showPassword,setShowPassword] = useState(false)

    const [showConfirmPassword,setShowConfirmPassword] = useState(false)
    const[data,setData] = useState({
             name:"",
            email:"",
            password:"",
            passwordConfirm:"",
            profilePic:""
        })

        const navigate = useNavigate()
    const handleOnChange = (e) =>{
        const {name,value} = e.target
        setData((preve)=>{
            return {
                ...preve,
                [name]:value

            }

        })

    }

    const handleUploadPic= async(e)=> {
        const file = e.target.files[0]

        const imagePic = await imageTobase64(file)

      
        setData((preve)=>{
            return{
                ...preve,
                profilePic:imagePic

            }
        })
       

    }

    const handleSubmit = async(e)=>{
        e.preventDefault()

        if(data.password === data.passwordConfirm){
            const dataResponse = await fetch(SummaryApi.signup.url,{
                method:SummaryApi.signup.method,
                headers:{
                    "content-type":"application/json",
                },
                body:JSON.stringify(data)
            })
            const dataApi = await dataResponse.json()
            if(dataApi.success){
                toast.success(dataApi.message)
                navigate('/login')
            }
            if(dataApi.error)
            {
                toast.error(dataApi.message)
            }
           
           

        }else{
            console.log("please check password and confirm password")
        }

       

    }

    console.log(data)
  return (
    <section id='signUp'>
        <div className=' mx-auto container p-4'>
            <div className='bg-white  p-5 w-full max-w-sm mx-auto '>
                <div className='w-20 h-20 mx-auto relative overflow-hidden rounded-full'>
                    <div>
                    <img src={data.profilePic || loginIcons} alt='login icon'/>
                    </div>
                    <form>
                        <label>
                        <div className='text-xs bg-slate-200 bg-opacity-80 pb-4 pt-2 text-center cursor-pointer absolute bottom-0 w-full'>
                    Upload photo

                    </div>
                            <input type='file' className='hidden' onChange={handleUploadPic}/>
                        </label>
                   

                    </form>
                   
                   

                </div>
                <form className=' pt-6 flex flex-col gap-2' onSubmit={handleSubmit}>

                <div className=' grid'>
                        <label>Name:</label>
                        <div className='bg-slate-100 p-2'>
                        <input className='w-full h-full outline-none bg-transparent'
                         type='text'
                         required 
                         name='name'
                         value={data.name}
                         onChange={handleOnChange}
                         placeholder='Enter your name'/>
                        </div>
                       
                    </div>
                    <div className=' grid'>
                        <label>Email:</label>
                        <div className='bg-slate-100 p-2'>
                        <input className='w-full h-full outline-none bg-transparent'
                         type='email' 
                         required
                         name='email'
                         value={data.email}
                         onChange={handleOnChange}
                         placeholder='Enter your email'/>
                        </div>
                       
                    </div>

                    <div>
                        <label>Password:</label>
                        <div className='bg-slate-100 p-2 flex'>
                        <input className='w-full h-full outline-none bg-transparent'
                         type={showPassword ? "text":"password" }
                         name='password'
                         required
                         value={data.password}
                         onChange={handleOnChange}
                          placeholder='Enter your password'/>
                        <div className=' cursor-pointer text-xl' onClick={()=>setShowPassword((preve)=>!preve)}>
                        <span>
                            {showPassword ?(<FaEyeSlash/>) :( <FaEye />)}
                        </span>
                        </div>
                        </div>
                            
                    </div>


                    <div>
                        <label>Confirm Password:</label>
                        <div className='bg-slate-100 p-2 flex'>
                        <input className='w-full h-full outline-none bg-transparent'
                         type={showConfirmPassword ? "text":"password" }
                         name='passwordConfirm'
                         required
                         value={data.passwordConfirm}
                         onChange={handleOnChange}
                          placeholder='Enter your password confirmation'/>
                        <div className=' cursor-pointer text-xl' onClick={()=>setShowConfirmPassword((preve)=>!preve)}>
                        <span>
                            {showConfirmPassword ?(<FaEyeSlash/>) :( <FaEye />)}
                        </span>
                        </div>
                        </div>
                                
                    </div>
                    <button className=' bg-red-600 hover:bg-red-700 text-white px-6 py-2 w-ful max-w-[150px] rounded-full hover:scale-110 transition-all mx-auto block mt-6'>Register</button>

                </form>
                <p className='my-4'>Already have an account? <Link to={'/login'} className='text-red-600 hover:text-red-700 hover:underline'>Login here</Link></p>
            </div>

        </div>

    </section>
  )
}

export default SignUp

import React from 'react'

const Footer = () => {
  return (
   <footer className=' bg-slate-300'>
    <div className='container mx-auto p-4'>
    <p className=' text-center font-bold'>DLD_Verse Toy shop &copy; 2024</p>
    </div>

   </footer>
  )
}

export default Footer
